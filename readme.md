
This is the repository of the URDF2KinDSL tool. The URDF2KinDSL is a converter
from the [URDF][urdf-web] to the [Kinematics-DSL][kindsl-web] robot model
formats.

# Usage
Please refer to the command-line help:

```
./urdf2kindsl.py --help
```

# Requirements

* Python (2.7 or 3, both _should_ work)
* NumPy for Python

# Limitations

TODO...


# Copyright notice

Copyright © 2018 Marco Frigerio

Released under the BSD 2-clause license. See the `LICENSE` file for additional
information.

[urdf-web]: http://wiki.ros.org/urdf/XML
[kindsl-web]: https://robcogenteam.bitbucket.io/rmodel.html
